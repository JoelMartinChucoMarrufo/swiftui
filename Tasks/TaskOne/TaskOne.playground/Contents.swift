
enum Symbol: String {
    case sum    = "+"
    case subtraction   = "-"
    case multiplication    = "*"
    case division = "/"
}

struct Operation {
    
    var symbol: Symbol
    
    init(symbol: Symbol) {
        self.symbol = symbol
    }
    
    func executeOperation(valueA: String, valueB: String) {

        guard let valueOne = Float(valueA) else {
            return print("Debe de ingresar un número válido en el valor A")
        }
        
        guard let valueTwo = Float(valueB) else {
            return print("Debe de ingresar un número válido en el valor B")
        }
        
        var result: Float = .zero
        switch symbol {
        case .sum:
            result = valueOne + valueTwo
        case .subtraction:
            result = valueOne - valueTwo
        case .multiplication:
            result = valueOne * valueTwo
        case .division:
            if (valueTwo.isEqual(to: .zero)) {
                return print("No se puede realizar división entre CERO")
            } else {
                result = valueOne / valueTwo
            }
        }
        print("El resultado de la operacion es \(result)")
    }
}

let operation = Operation(symbol: .division)
operation.executeOperation(valueA: "10.5", valueB: "8")


