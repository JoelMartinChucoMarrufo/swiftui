//
//  CastCrewAvatar.swift
//  TaskThree
//
//  Created by Joel Martin Chuco Marrufo on 30/05/21.
//

import SwiftUI

struct CastCrewAvatar: View {
    
    var imageName: String
    var name: String
    var lastName: String
    var distribution: String
    
    var body: some View {
        VStack {
            Image(imageName)
                .resizable()
                .aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/)
                .frame(width: 66, height: 66, alignment: .top)
                .clipShape(Circle())
            
            Text(name)
                .font(.system(size: 13))
                .bold()
                .foregroundColor(Color(red: 29/255, green: 29/255, blue: 65/255))
            
            Text(lastName)
                .font(.system(size: 13))
                .bold()
                .foregroundColor(Color(red: 29/255, green: 29/255, blue: 65/255))
            
            Text(distribution)
                .font(.system(size: 12))
                .foregroundColor(Color(red: 163/255, green: 164/255, blue: 181/255))
                .padding(.top, 1)
        }
    }
}

struct CastCrewAvatar_Previews: PreviewProvider {
    static var previews: some View {
        CastCrewAvatar(imageName: "JamesMangold", name: "James", lastName: "Mangold", distribution: "Director")
    }
}
