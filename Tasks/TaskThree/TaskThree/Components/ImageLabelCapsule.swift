//
//  ImageLabelCapsule.swift
//  TaskThree
//
//  Created by Joel Martin Chuco Marrufo on 30/05/21.
//

import SwiftUI

struct ImageLabelCapsule: View {
    var image: String
    var label: String
    
    var body: some View {
        
        Button(action: {}) {
            HStack {
                Image(systemName: image)
                    .font(.system(size: 22))
                Text(label)
                    .fontWeight(.bold)
                    .font(.system(size: 16))
            }
            .padding(12)
            .foregroundColor(.white)
            .background(Color(red: 108/255, green: 69/255, blue: 189/255))
            .cornerRadius(40)
        }
        
    }
}

struct ImageLabelCapsule_Previews: PreviewProvider {
    static var previews: some View {
        ImageLabelCapsule(image: "star", label: "LabelCapsule")
    }
}
