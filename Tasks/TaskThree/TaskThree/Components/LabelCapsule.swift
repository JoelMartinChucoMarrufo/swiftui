//
//  LabelCapsule.swift
//  TaskThree
//
//  Created by Joel Martin Chuco Marrufo on 30/05/21.
//

import SwiftUI

struct LabelCapsule: View {
    var text: String
    
    var body: some View {
        Text(text)
            .font(.system(size: 13))
            .bold()
            .foregroundColor(Color(red: 106/255, green: 105/255, blue: 131/255))
            .padding(.init(top: 8, leading: 18, bottom: 8, trailing: 18))
            .overlay(
                RoundedRectangle(cornerRadius: 20)
                    .stroke(Color(UIColor.lightGray), lineWidth: 1)
            )
    }
}

struct LabelCapsule_Previews: PreviewProvider {
        
    static var previews: some View {
        LabelCapsule(text: "LabelCapsule")
    }
}
