//
//  TaskThreeApp.swift
//  TaskThree
//
//  Created by Joel Martin Chuco Marrufo on 30/05/21.
//

import SwiftUI

@main
struct TaskThreeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
