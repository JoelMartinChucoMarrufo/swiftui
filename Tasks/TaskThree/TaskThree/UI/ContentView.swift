//
//  ContentView.swift
//  TaskThree
//
//  Created by Joel Martin Chuco Marrufo on 30/05/21.
//

import SwiftUI

struct ContentView: View {
    
    
    var body: some View {
        
        NavigationView {
            VStack{
                NavigationLink(destination: FordFerrariView()){
                    Text("Ford v Ferrari")
                }.padding(.bottom, 24)
                
                NavigationLink(destination: MusselesView()){
                    Text("Musseles")
                }
            }
        }
        
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .previewDevice("iPhone 12 Pro")
    }
}
