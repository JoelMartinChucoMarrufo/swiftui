//
//  FordFerrariView.swift
//  TaskThree
//
//  Created by Joel Martin Chuco Marrufo on 30/05/21.
//

import SwiftUI

struct FordFerrariView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        
        ZStack {
            VStack (alignment: .leading)  {
                ZStack {
                    Image("FordFerrari")
                        .resizable()
                        .scaledToFill()
                        .frame(height: 260, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .clipped()
                        .mask(RoundedCorners(tl: 0, tr: 0, bl: 48, br: 0))
                        .padding(.bottom, 48)
                    
                    HStack {
                        Color.white
                    }
                    .frame(height: 96)
                    .mask(RoundedCorners(tl: 48, tr: 0, bl: 48, br: 0))
                    .padding(.leading, 30)
                    .padding(.top, 212)
                    .shadow(color: Color(red: 228/255, green: 226/255, blue: 232/255), radius: 8, x: 4, y: 16)
                    
                    HStack {
                        VStack {
                            Image(systemName: "star.fill")
                                .foregroundColor(.yellow)
                                .font(.system(size: 20))
                                .padding(.bottom, 2)
                            Text("8.2/10")
                                .font(.system(size: 13))
                                .foregroundColor(.black)
                            Text("150,212")
                                .font(.system(size: 10))
                                .foregroundColor(Color(UIColor.lightGray))
                        }
                        .padding(.top, 8)
                        .padding(.bottom, 8)
                        
                        Spacer()
                        
                        VStack {
                            Image(systemName: "star")
                                .foregroundColor(.black)
                                .font(.system(size: 20))
                                .padding(.bottom, 2)
                            Text("Rate This")
                                .font(.system(size: 13))
                                .foregroundColor(.black)
                            Text("")
                        }
                        .padding(.top, 8)
                        .padding(.bottom, 8)
                        
                        Spacer()
                        
                        VStack {
                            Text("86")
                                .padding(.init(top: 4, leading: 6, bottom: 4, trailing: 6))
                                .font(.system(size: 12))
                                .foregroundColor(.white)
                                .background(Color.green)
                            Text("Metascore")
                                .font(.system(size: 13))
                                .foregroundColor(.black)
                            Text("62 critic reviews")
                                .font(.system(size: 10))
                                .foregroundColor(Color(UIColor.lightGray))
                        }
                        .padding(.top, 8)
                        .padding(.bottom, 8)
                    }
                    .frame(height: 96)
                    .padding(.leading, 86)
                    .padding(.top, 212)
                    .padding(.trailing, 36)

                }
                
                HStack {
                    VStack (alignment: .leading) {
                        Text("Ford v Ferrari")
                            .font(.system(size: 26))
                            .bold()
                        HStack {
                            Text("2019")
                                .padding(.init(top: 0, leading: 0, bottom: 0, trailing: 11))
                            Text("PG-13")
                                .padding(.init(top: 0, leading: 0, bottom: 0, trailing: 11))
                            Text("2h 32min")
                        }
                        .font(.system(size: 12))
                        .foregroundColor(Color(UIColor.lightGray))
                        .padding(.top, 2)
                    }
                    
                    Spacer()
                    
                    Button(action: {}) {
                        Image(systemName: "plus")
                            .font(.title3)
                            .padding()
                            .background(Color(red: 255/255, green: 99/255, blue: 132/255))
                            .foregroundColor(.white)
                            .clipShape(RoundedRectangle(cornerRadius: 16))
                    }
                }
                .padding(.top, 30)
                .padding(.leading, 20)
                .padding(.trailing, 32)
                
                HStack {
                    LabelCapsule(text: "Action")
                    LabelCapsule(text: "Biography")
                    LabelCapsule(text: "Drama")
                    Spacer()
                }
                .padding(.top, 16)
                .padding(.leading, 20)
                .padding(.trailing, 32)
                
                Text("Plot Summary")
                    .font(.system(size: 20))
                    .padding(.leading, 20)
                    .padding(.top, 24)
                
                Text("American car designer Carrol Shelby and driver Kn Miles battle corporate interference and the laws of physics to build a revolutionary rare car for Ford in order.")
                    .font(.system(size: 13))
                    .foregroundColor(Color(red: 106/255, green: 105/255, blue: 131/255))
                    .frame(height: 64)
                    .padding(.leading, 20)
                    .padding(.top, 12)
                    .padding(.trailing, 20)
                    .lineSpacing(5)
                
                Text("Cast & Crew")
                    .font(.system(size: 20))
                    .padding(.leading, 20)
                    .padding(.top, 24)
                
                HStack {
                    CastCrewAvatar(imageName: "JamesMangold", name: "James", lastName: "Mangold", distribution: "Director")
                    Spacer()
                    CastCrewAvatar(imageName: "MattDamon", name: "Matt", lastName: "Damon", distribution: "Carroll")
                    Spacer()
                    CastCrewAvatar(imageName: "ChristianBale", name: "Christian", lastName: "Bale", distribution: "Ken Miles")
                    Spacer()
                    CastCrewAvatar(imageName: "CaitrionaBalfe", name: "Caitriona", lastName: "Balfe", distribution: "Mollie")
                }
                .padding(.init(top: 12, leading: 20, bottom: 20, trailing: 20))
                
                Spacer()
                
            }
            .ignoresSafeArea(edges: .top)
            
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: btnBack)
    }
    
    var btnBack : some View { Button(action: {
            self.presentationMode.wrappedValue.dismiss()
            }) {
                HStack {
                    Image(systemName: "chevron.left")
                        .resizable()
                        .foregroundColor(.black)
                        .frame(width: 8, height: 16)
                        .padding()
                }
            }
        }
}

struct FordFerrariView_Previews: PreviewProvider {
    static var previews: some View {
        FordFerrariView()
    }
}
