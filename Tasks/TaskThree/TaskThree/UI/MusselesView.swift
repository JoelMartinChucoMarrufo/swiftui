//
//  MusselesView.swift
//  TaskThree
//
//  Created by Joel Martin Chuco Marrufo on 30/05/21.
//

import SwiftUI

struct MusselesView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        ZStack {
            Color(red: 108/255, green: 69/255, blue: 189/255).edgesIgnoringSafeArea(.all)
            
            VStack {
                HStack {
                    Spacer()
                    Image(systemName: "heart").foregroundColor(.white).font(.system(size: 32))
                }.padding(.init(top: 52, leading: 0, bottom: 140, trailing: 20))
                
                
                
                ZStack (alignment: Alignment(horizontal: .center, vertical: .top)) {
                    Color.white.edgesIgnoringSafeArea(.bottom)
                    
                    VStack {
                        HStack {
                            Text("Musseles")
                                .font(.system(size: 36))
                                .foregroundColor(.black)
                                .bold()
                                .padding(.top)
                            Spacer()
                            HStack (alignment: .top) {
                                Text("$")
                                    .font(.system(size: 24))
                                    .bold()
                                    .padding(.init(top: 4, leading: 0, bottom: 0, trailing: 0))
                                Text("16,00")
                                    .font(.system(size: 40))
                                    .bold()
                            }.foregroundColor(Color(red: 254/255, green: 185/255, blue: 4/255))
                        }
                        .padding(.top, 160)
                        .padding(.leading, 32)
                        .padding(.trailing, 32)
                        
                        HStack {
                            ImageLabelCapsule(image: "timer", label: "30m")
                            ImageLabelCapsule(image: "star", label: "4.5")
                            Spacer()
                        }
                        .padding(.top, 16)
                        .padding(.leading, 32)
                        .padding(.trailing, 32)
                        
                        HStack {
                            Text("About")
                                .fontWeight(.semibold)
                                .font(.system(size: 24))
                                .foregroundColor(.black)
                                
                            Spacer()
                        }
                        .padding(.top, 32)
                        .padding(.leading, 32)
                        
                        HStack {
                            Text("In addition to the freshest seafood, there are corn, cilantro and tomatoes: their first became mayonnaise, the second-cream, the third - spicy tomato water.")
                                .font(.system(size: 16))
                                .foregroundColor(Color(UIColor.lightGray))
                                .lineSpacing(7)
                                
                            Spacer()
                        }
                        .padding(.top, 8)
                        .padding(.leading, 32)
                        .padding(.trailing, 32)
                        
                        Spacer()
                        
                        Button(action: {}, label: {
                            Text("Add to cart")
                                .font(.system(size: 20))
                                .bold()
                                .foregroundColor(.white)
                                .padding(.vertical)
                                .padding(.top, 8)
                                .padding(.bottom, 8)
                                .frame(maxWidth: .infinity)
                                .background(Color(red: 108/255, green: 69/255, blue: 189/255))
                                .clipShape(RoundedRectangle(cornerRadius: 28.0))
                        })
                        .padding(.top, 8)
                        .padding(.leading, 32)
                        .padding(.trailing, 32)
                        
                        Spacer()
                    }
                }.mask(RoundedCorners(tl: 40, tr: 40, bl: 0, br: 0)).ignoresSafeArea(edges: /*@START_MENU_TOKEN@*/.bottom/*@END_MENU_TOKEN@*/)
            }.ignoresSafeArea(edges: .top)
            
            Image("Musseles")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 260, height: 260)
                .cornerRadius(130)
                .shadow(color: .white, radius: 5, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/)
                .offset(x: 0, y: -240)

        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: btnBack)
    }
    
    var btnBack : some View { Button(action: {
            self.presentationMode.wrappedValue.dismiss()
            }) {
                HStack {
                    Image(systemName: "chevron.left")
                        .resizable()
                        .foregroundColor(.white)
                        .frame(width: 8, height: 16)
                        .padding()
                }
            }
        }
}

struct MusselesView_Previews: PreviewProvider {
    static var previews: some View {
        MusselesView()
            .previewDevice("iPhone 11")
    }
}
