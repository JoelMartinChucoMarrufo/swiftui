//
//  ContentView.swift
//  TaskTwo
//
//  Created by Joel Martin Chuco Marrufo on 22/05/21.
//

import SwiftUI

private var operationSymbolToCalculate: Symbol = .none
private var valueA = ""

struct ContentView: View {
    
    var backgroundColorGray: Color = Color(red: 155/255, green: 155/255, blue: 155/255)
    var backgroundColorOrange: Color = Color(red: 239/255, green: 154/255, blue: 65/255)
    var backgroundColorDarkGray: Color = Color(red: 45/255, green: 45/255, blue: 45/255)
    
    @State var value: String = "0"
    @State var textSize: CGFloat = 96
    @State var operationSymbol: Symbol = .none
    
    var body: some View {
        ZStack {
            Color.black.edgesIgnoringSafeArea(.all)
            VStack {
                Spacer()
                HStack {
                    Text(value).foregroundColor(.white).font(.system(size: textSize)).lineLimit(1)
                }.padding().frame(width: 380, height: 120, alignment: .trailing)
                HStack {
                    ButtonWithTextInCircle(backgroundColor: backgroundColorGray, textValue: "AC", textColor: .black, value: $value, textSize: $textSize, operationSymbol: $operationSymbol)
                    ButtonWithImageInCircle(backgroundColor: backgroundColorGray, imageValue: "plus.slash.minus", imageColor: .black,  symbol: .plusAndMinus, operationSymbol: $operationSymbol, valueResult: $value)
                    ButtonWithImageInCircle(backgroundColor: backgroundColorGray, imageValue: "percent", imageColor: .black,   symbol: .percent, operationSymbol: $operationSymbol, valueResult: $value)
                    ButtonWithImageInCircleForOperation(backgroundColor: backgroundColorOrange, imageValue: "divide", imageColor: .white,  symbol: .divide, operationSymbol: $operationSymbol, valueResult: $value)
                }.padding(.bottom)
                HStack {
                    ButtonWithTextInCircle(backgroundColor: backgroundColorDarkGray, textValue: "7", textColor: .white, value: $value, textSize: $textSize, operationSymbol: $operationSymbol)
                    ButtonWithTextInCircle(backgroundColor: backgroundColorDarkGray, textValue: "8", textColor: .white, value: $value, textSize: $textSize, operationSymbol: $operationSymbol)
                    ButtonWithTextInCircle(backgroundColor: backgroundColorDarkGray, textValue: "9", textColor: .white, value: $value, textSize: $textSize, operationSymbol: $operationSymbol)
                    ButtonWithImageInCircleForOperation(backgroundColor: backgroundColorOrange, imageValue: "multiply", imageColor: .white,  symbol: .multiply, operationSymbol: $operationSymbol, valueResult: $value)
                }.padding(.bottom)
                HStack {
                    ButtonWithTextInCircle(backgroundColor: backgroundColorDarkGray, textValue: "4", textColor: .white, value: $value, textSize: $textSize, operationSymbol: $operationSymbol)
                    ButtonWithTextInCircle(backgroundColor: backgroundColorDarkGray, textValue: "5", textColor: .white, value: $value, textSize: $textSize, operationSymbol: $operationSymbol)
                    ButtonWithTextInCircle(backgroundColor: backgroundColorDarkGray, textValue: "6", textColor: .white, value: $value, textSize: $textSize, operationSymbol: $operationSymbol)
                    ButtonWithImageInCircleForOperation(backgroundColor: backgroundColorOrange, imageValue: "minus", imageColor: .white,  symbol: .minus, operationSymbol: $operationSymbol, valueResult: $value)
                }.padding(.bottom)
                HStack {
                    ButtonWithTextInCircle(backgroundColor: backgroundColorDarkGray, textValue: "1", textColor: .white, value: $value, textSize: $textSize, operationSymbol: $operationSymbol)
                    ButtonWithTextInCircle(backgroundColor: backgroundColorDarkGray, textValue: "2", textColor: .white, value: $value, textSize: $textSize, operationSymbol: $operationSymbol)
                    ButtonWithTextInCircle(backgroundColor: backgroundColorDarkGray, textValue: "3", textColor: .white, value: $value, textSize: $textSize, operationSymbol: $operationSymbol)
                    ButtonWithImageInCircleForOperation(backgroundColor: backgroundColorOrange, imageValue: "plus", imageColor: .white,  symbol: .plus, operationSymbol: $operationSymbol, valueResult: $value)
                }.padding(.bottom)
                HStack {
                    ButtonWithTextInCapsule(backgroundColor: backgroundColorDarkGray, textValue: "0", textColor: .white, value: $value, textSize: $textSize, operationSymbol: $operationSymbol)
                    HStack {
                        ButtonWithTextInCircle(backgroundColor: backgroundColorDarkGray, textValue: ".", textColor: .white, value: $value, textSize: $textSize, operationSymbol: $operationSymbol)
                        ButtonWithImageInCircle(backgroundColor: backgroundColorOrange, imageValue: "equal", imageColor: .white,  symbol: .equal, operationSymbol: $operationSymbol, valueResult: $value)
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
                .previewDevice("iPhone 12 Pro Max")
            ContentView()
                .previewDevice("iPhone SE (2nd generation)")
        }
    }
}

struct ButtonWithTextInCircle: View {
    
    var backgroundColor: Color
    var textValue: String
    var textColor: Color
    @Binding var value: String
    @Binding var textSize: CGFloat
    @Binding var operationSymbol: Symbol
    
    var body: some View {
        Button(textValue, action: {
            if (textValue == "AC") {
                value = "0"
                valueA = ""
                operationSymbolToCalculate = .none
                operationSymbol = .none
            } else if (value.count < 21) {
                value = addNewValueInValueResult(value: textValue, valueResult: value, operationSymbol: operationSymbol)
                textSize = obtainNewSizeForValueResult(valueResult: value)
                if (operationSymbol != .none) {
                    operationSymbolToCalculate = operationSymbol
                    operationSymbol = .none
                }
            }
        })
    .foregroundColor(textColor)
    .font(.title)
    .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
    .background(backgroundColor)
    .clipShape(Circle())
}
}

struct ButtonWithTextInCapsule: View {
    
    var backgroundColor: Color
    var textValue: String
    var textColor: Color
    @Binding var value: String
    @Binding var textSize: CGFloat
    @Binding var operationSymbol: Symbol
    
    var body: some View {
        Button(textValue, action: {
            if (value.count < 21) {
                if (operationSymbol != .none) {
                    operationSymbolToCalculate = operationSymbol
                    operationSymbol = .none
                }
                value = addNewValueInValueResult(value: textValue, valueResult: value, operationSymbol: operationSymbol)
                textSize = obtainNewSizeForValueResult(valueResult: value)
            }
        })
            .padding(40)
            .foregroundColor(textColor)
            .font(.title)
            .frame(width: 172, height: 80, alignment: .leading)
            .background(backgroundColor)
            .clipShape(Capsule())
    }
}

struct ButtonWithImageInCircleForOperation: View {
    
    var backgroundColor: Color
    var imageValue: String
    var imageColor: Color
    var symbol: Symbol
    @Binding var operationSymbol: Symbol
    @Binding var valueResult: String
    
    var body: some View {
        Button(action: {
            if (operationSymbol != .none) {
                let operation = Operation(symbol: symbol)
                valueResult = operation.executeOperation(valueA: valueA, valueB: valueResult)
            } else {
                valueA = valueResult
            }
            operationSymbol = symbol
            
        }) {
            Image(systemName: imageValue).font(.title).accentColor(symbol == operationSymbol ? backgroundColor : imageColor)
        }
        .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        .background(symbol == operationSymbol ? imageColor : backgroundColor)
        .clipShape(Circle())
    }
}

struct ButtonWithImageInCircle: View {
    
    var backgroundColor: Color
    var imageValue: String
    var imageColor: Color
    var symbol: Symbol
    @Binding var operationSymbol: Symbol
    @Binding var valueResult: String
    
    var body: some View {
        Button(action: {
            if (operationSymbolToCalculate != .none) {
                let operation = Operation(symbol: operationSymbolToCalculate)
                valueResult = operation.executeOperation(valueA: valueA, valueB: valueResult)
                valueA = ""
                operationSymbol = symbol
                operationSymbolToCalculate = .none
            } else if (symbol == .percent || symbol == .plusAndMinus) {
                let operation = Operation(symbol: symbol)
                valueResult = operation.executeOperation(valueA: valueA, valueB: valueResult)
            }
        }) {
            Image(systemName: imageValue).font(.title).accentColor(imageColor)
        }
        .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        .background(backgroundColor)
        .clipShape(Circle())
    }
}

func addNewValueInValueResult(value: String, valueResult: String, operationSymbol: Symbol) -> String {
    var valueChanged = valueResult
    
    if (operationSymbol == .none) {
        switch value {
        case "AC": valueChanged = "0"
        case ".": if (!valueResult.contains(".")) {
            valueChanged.append(value)
        }
        default: if (valueResult != "0" || (value == "." && valueResult == "0")) {
            valueChanged.append(value)
        } else {
            valueChanged = value
        }
        }
    } else {
        valueChanged = value
    }
    
    return valueChanged
}

func obtainNewSizeForValueResult(valueResult: String) -> CGFloat {
    var textSize: CGFloat = 96
    
    if (valueResult.count <= 6) {
        textSize = 96
    } else if (valueResult.count <= 8) {
        textSize = 72
    } else if (valueResult.count <= 10) {
        textSize = 60
    } else if (valueResult.count <= 12) {
        textSize = 48
    } else if (valueResult.count <= 16) {
        textSize = 36
    } else if (valueResult.count <= 21) {
        textSize = 28
    }
    
    return textSize
}

enum Symbol: String {
    case plus    = "+"
    case minus   = "-"
    case multiply    = "*"
    case divide = "/"
    case percent = "%"
    case plusAndMinus = "+/-"
    case equal = "="
    case none = ""
}

struct Operation {
    
    var symbol: Symbol
    
    init(symbol: Symbol) {
        self.symbol = symbol
    }
    
    func executeOperation(valueA: String, valueB: String) -> String{

        guard let valueOne = (symbol == .percent || symbol == .plusAndMinus) ? Float.zero : Float(valueA) else {
            return "ERROR"
        }
        
        guard let valueTwo = Float(valueB) else {
            return "ERROR"
        }
        
        var result: Float = .zero
        switch symbol {
        case .plus:
            result = valueOne + valueTwo
        case .minus:
            result = valueOne - valueTwo
        case .multiply:
            result = valueOne * valueTwo
        case .divide:
            if (valueTwo.isEqual(to: .zero)) {
                return "ERROR"
            } else {
                result = valueOne / valueTwo
            }
        case .percent:
            result = valueTwo / 100
        case .plusAndMinus:
            if (valueB.contains("-")) {
                return valueB.replacingOccurrences(of: "-", with: "")
            } else {
                return "-".appending(valueB)
            }
        case .equal:break
        case .none:
            return String(result)
        }
        let valueInt: Int = Int(result)
        let difference = result - Float(valueInt)
        return (difference > 0.0 ? String(result) : String(valueInt))
    }
}

