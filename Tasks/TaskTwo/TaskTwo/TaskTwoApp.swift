//
//  TaskTwoApp.swift
//  TaskTwo
//
//  Created by Joel Martin Chuco Marrufo on 22/05/21.
//

import SwiftUI

@main
struct TaskTwoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
